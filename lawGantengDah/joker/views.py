from django.shortcuts import render
import requests


# Create your views here.
def index(request):
    url = 'https://sv443.net/jokeapi/v2/joke/{}?blacklistFlags=nsfw&type=single'
    if request.method == 'POST':
        category = request.POST.get('inputRadio')
        the_joke = requests.get(url.format(category)).json() #request the API data and convert the JSON to Python data types
    else:
        category = 'Any'
        the_joke = requests.get(url.format(category)).json() #request the API data and convert the JSON to Python data types

    joke = {
        'joke' : the_joke['joke']
    }

    context = {'jokeInput' : the_joke['joke']}

    return render(request, 'joker/index.html',context=context) #returns the index.html template